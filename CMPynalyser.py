"""CMPynalyser aims at computing several genetic and demographic indicators
from the outputs of the CropMetaPop model"""

import sys
import os
import time
import pandas as pd
import numpy as np

if pd.__version__ < "2.1.2":
    sys.exit("Please upgrade pandas to a version >= 2.1.2. You can generally use: pip3 install -U pandas")
if np.__version__ < "1.26.1":
    sys.exit("Please upgrade numpy to a version >= 1.26.1. You can generally use: pip3 install -U numpy")


start_time = time.time()

PATH = sys.argv[1]
rawData = pd.read_csv(os.path.join(PATH, "GenotypeMono.csv"))


def get_simu_infos(data, nb_m_sel):
    """Get some simulation informations from the data file"""
    NB_REPS = max(rawData["Replicate"]) + 1
    NB_POPS = max(rawData["Population"]) + 1
    NB_MARKS = max(rawData["Marker"]) + 1
    NB_GENS = int(rawData.columns[len(data.columns) - 1].split(" ")[2]) + 1

    return [NB_REPS, NB_POPS, NB_MARKS, int(nb_m_sel), NB_GENS]


NB_MKSEL = sys.argv[2]

try:
    POPS_TO_GET = [int(i) for i in sys.argv[3].split(",")]
    print("Treating the following populations: " +
          ", ".join([str(i) for i in POPS_TO_GET]))
except Exception:
    if sys.argv[3] == "p":
        POPS_TO_GET = [i for i in range(get_simu_infos(rawData, NB_MKSEL)[1])]
        print("Treating the following populations: " +
              ", ".join([str(i) for i in POPS_TO_GET]))
    else:
        sys.exit("Missing information of populations to treat")

try:
    MKS_TO_GET = [int(i) for i in sys.argv[4].split(",")]
    print("Treating the following markers: " +
          ", ".join([str(i) for i in MKS_TO_GET]))
except Exception:
    if sys.argv[4] == "m":
        MKS_TO_GET = [i for i in range(get_simu_infos(rawData, NB_MKSEL)[2])]
        print("Treating the following markers: " +
              ", ".join([str(i) for i in MKS_TO_GET]))
    else:
        sys.exit("Missing information of markers to treat")

try:
    MOD_GENS = int(sys.argv[5])
    LST_GENS = [i for i in range(get_simu_infos(rawData, NB_MKSEL)[4]) if
                i % MOD_GENS == 0 or i == max(range(get_simu_infos(rawData,
                                                             NB_MKSEL)[4]))]
    print("Treating every " + sys.argv[5] + " generations:" +
          ", ".join([str(i) for i in LST_GENS]))
except Exception:
    if sys.argv[5] == "g":
        LST_GENS = [i for i in range(get_simu_infos(rawData, NB_MKSEL)[4])]
        print("Treating every generations: " +
              ", ".join([str(i) for i in LST_GENS]))
    else:
        sys.exit("Missing information of generations to treat")

NB_STEPS_HS = get_simu_infos(rawData, NB_MKSEL)[0] * len(POPS_TO_GET) *\
    len(MKS_TO_GET) * len(LST_GENS)


def format_simu_infos(data, nb_m_sel):
    """Formats the simulation informations for display"""
    infos = get_simu_infos(data, nb_m_sel)
    print("Nb reps : " + str(infos[0]) + "\nNb pops : " + str(infos[1]) +
          "\nNb markers : " + str(infos[2]) + "\nNb marks Sel : " +
          str(infos[3]) + "\nNb generations : " + str(infos[4]))


def check_all_marker_combinations(data, nb_m_sel):
    """Check if all marker combinations are present in the data file"""
    infos = get_simu_infos(data, nb_m_sel)

    nb_rows = len(data)

    print(len(data), infos[0] * infos[1] * infos[2] * 3)

    if nb_rows == infos[0] * infos[1] * infos[2]:
        return True
    else:
        return False


def find_heterozygotes(sample):
    """Find indexes of heterozygotes"""
    res = [ind for ind, i in enumerate(sample["Genotype"])
           if i.split("/")[0] != i.split("/")[1]]
    return res


def find_homozygotes(sample):
    """Find indexes of homozygotes"""
    res = [ind for ind, i in enumerate(sample["Genotype"])
           if i.split("/")[0] == i.split("/")[1]]
    return res


def find_all_rich(mkCombs, sample, list_alleles):
    """compute allele richness"""
    sampleTemp = {
        "Geno": mkCombs,
        "eff": sample
    }
    mysample = pd.concat(sampleTemp, axis=1)
    allTabTemp = {
        "all": pd.Series(list_alleles),
        "pre": pd.Series((0 for i in range(0, len(list_alleles))))
    }
    allTab = pd.concat(allTabTemp, axis=1)
    for i in range(0, len(mysample)):
        myline = mysample.iloc[i]
        if myline["eff"] > 0:
            genos = myline["Geno"].split("/")
            for geno in genos:
                allTab.loc[allTab["all"] == geno, "pre"] += 1

    sumAlleles = 0
    for i in range(0, len(allTab)):
        if allTab.iloc[i]["pre"] > 0:
            sumAlleles += 1

    return sumAlleles


def calcul_hs_hobs(data, nb_m_sel, nb_steps_hs):
    """Compute Hs, Hobs, Fis, Q1, allele richness and evenness"""
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbMarks = int(infos[2])
    nbMkSel = int(infos[3])
    nbGens = infos[4]

    colGens = [i for i in data.columns if "Gen " in i]

    sel_nsel = ["Sel" if x < nbMkSel else "NSel" for x in [i for i in
                                                           range(nbMarks)]]
    hobs = pd.DataFrame(columns=["Rep", "Pop", "Mk", "SelSt", "Gen", "Hobs"])
    hs = pd.DataFrame(columns=["Hs"])
    fis = pd.DataFrame(columns=["Fis"])
    q1 = pd.DataFrame(columns=["Q1"])
    allrich = pd.DataFrame(columns=["AllRich"])
    evenness = pd.DataFrame(columns=["Evenness"])
    my_step = 0
    for nrep in range(0, nbReps):
        for npop in range(0, nbPops):
            if npop in POPS_TO_GET:
                for nmk in range(0, nbMarks):
                    if nmk in MKS_TO_GET:
                        mysample = data[(data["Replicate"] == nrep) &
                                         (data["Population"] == npop) &
                                         (data["Marker"] == nmk)]
                        mk_combs = mysample["Genotype"]
                        list_alleles = [i for i in
                                    np.unique("/".join(mk_combs).split("/"))]
                        heterozygotes = find_heterozygotes(mysample)
                        homozygotes = find_homozygotes(mysample)

                        genValues = pd.DataFrame(columns=["gt1", "gt2",
                                                            "value"])
                        for gen1 in mk_combs:
                            for gen2 in mk_combs:
                                valID =\
                                    int(gen1.split("/")[0] ==
                                        gen2.split("/")[0]) +\
                                    int(gen1.split("/")[0] ==
                                        gen2.split("/")[1]) +\
                                    int(gen1.split("/")[1] ==
                                        gen2.split("/")[0]) +\
                                    int(gen1.split("/")[1] ==
                                        gen2.split("/")[1])
                                ligne = pd.DataFrame({"gt1": gen1, "gt2": gen2,
                                         "value": valID},
                                         index=[0])
                                genValues = pd.concat([genValues, ligne],
                                                             ignore_index=True)

                        for ngen in range(0, nbGens):
                            if ngen in LST_GENS:
                                mygen = mysample[colGens[ngen]]
                                total_inds = mygen.sum()

                                myAllRich = find_all_rich(mk_combs, mygen,
                                                          list_alleles)
                                ligne = pd.DataFrame({"AllRich": myAllRich}, index=[0])
                                allrich = pd.concat([allrich, ligne],
                                                         ignore_index=True)

                                ###
                                # Hobs
                                if total_inds == 0:
                                    myhobs = np.nan
                                    ligne = pd.DataFrame({"Rep": nrep, "Pop": npop,
                                             "Mk": nmk,
                                             "SelSt": sel_nsel[nmk],
                                             "Gen": ngen, "Hobs": myhobs},
                                             index=[0])
                                    hobs = pd.concat([hobs,ligne],
                                                       ignore_index=True)
                                else:
                                    if len(heterozygotes) >= 1:
                                        myhobs =\
                                            mygen.iloc[heterozygotes].sum() /\
                                        total_inds
                                        ligne = pd.DataFrame({"Rep": nrep, "Pop": npop,
                                                 "Mk": nmk,
                                                 "SelSt": sel_nsel[nmk],
                                                 "Gen": ngen, "Hobs": myhobs},
                                                 index=[0])
                                        hobs = pd.concat([hobs, ligne],
                                                           ignore_index=True)
                                    else:
                                        myhobs = 0
                                        ligne = pd.DataFrame({"Rep": nrep, "Pop": npop,
                                                 "Mk": nmk,
                                                 "SelSt": sel_nsel[nmk],
                                                 "Gen": ngen, "Hobs": myhobs},
                                                 index=[0])
                                        hobs = pd.concat([hobs, ligne],
                                                           ignore_index=True)

                                ###
                                # Hs + Evenness
                                if total_inds == 0:
                                    myhs = np.nan
                                    ligne = pd.DataFrame({"Hs": myhs},
                                            index=[0])
                                    hs = pd.concat([hs, ligne], ignore_index=True)
                                    evenness = pd.concat([evenness,
                                        pd.DataFrame({"Evenness":
                                        np.nan}, index=[0])],
                                                    ignore_index=True)
                                else:
                                    pi_pas_carres = []
                                    for allele in list_alleles:
                                        index_with_allele =\
                                            [ind for ind, i in
                                             enumerate(mk_combs) if allele in
                                             i.split("/")]
                                        somme_indiv = 0
                                        for ind in index_with_allele:
                                            if ind in homozygotes:
                                                # si ce sont les homozygotes
                                                somme_indiv += 2 *\
                                                mygen.iloc[ind]
                                            elif ind in heterozygotes:
                                                # si ce sont des heterozygotes
                                                somme_indiv += mygen.iloc[ind]
                                            else:
                                                # sinon...
                                                print("Something went wrong")
                                        mypi = somme_indiv / (2 * total_inds)
                                        pi_pas_carres.append(mypi)
                                    pi_no_zero = [i for i in pi_pas_carres if i
                                                  != 0]
                                    evenTemp = - sum([i * np.log(i) for i in
                                                      pi_no_zero]) /\
                                                      np.log(len(list_alleles))
                                    pi_carres = [i * i for i in pi_pas_carres]
                                    myhs = 1 - np.nansum(pi_carres)
                                    ligne = pd.DataFrame({"Hs": myhs},
                                            index=[0])
                                    hs = pd.concat([hs, ligne], ignore_index=True)
                                    evenness = pd.concat([evenness,
                                        pd.DataFrame({"Evenness":evenTemp},
                                            index=[0])] , ignore_index=True)

                                ###
                                # Fis
                                if total_inds == 0:
                                    myfis = np.nan
                                    ligne = pd.DataFrame({"Fis": myfis},
                                    index=[0])
                                    fis = pd.concat([fis,ligne], ignore_index=True)
                                else:
                                    try:
                                        myfis = 1 - (myhobs / myhs)
                                    except Exception:
                                        myfis = np.nan
                                    if np.isnan(myfis):
                                        myfis = 1
                                    ligne = pd.DataFrame({"Fis": myfis},
                                            index=[0])
                                    fis = pd.concat([fis, ligne], ignore_index=True)

                                ###
                                # Q1

                                sumTests = 0
                                nbId = 0
                                genNb = colGens[ngen]
                                myGenBett = mysample[["Genotype", genNb]]
                                for gen1 in mk_combs:
                                    for gen2 in mk_combs:
                                        if gen1 == gen2:
                                            gval = int(genValues[(genValues["gt1"] ==
                                                gen1) & (genValues["gt2"] ==
                                                    gen2)]["value"].iloc[0])
                                            eff =\
                    int(myGenBett[myGenBett["Genotype"] == gen1][genNb].iloc[0]) *\
                    int((myGenBett[myGenBett["Genotype"] == gen1][genNb].iloc[0]) - 1)
                                            sumTests += eff * 4
                                            nbId += gval * eff
                                        elif gen1 != gen2:
                                            gval = int(
                                genValues[(genValues["gt1"] == gen1) &
                                          (genValues["gt2"] == gen2)]["value"].iloc[0])
                                            eff =\
                        int(myGenBett[myGenBett["Genotype"] == gen1][genNb].iloc[0]) *\
                        int(myGenBett[myGenBett["Genotype"] == gen2][genNb].iloc[0])
                                            sumTests += eff * 4
                                            nbId += gval * eff
                                        else:
                                            print("Something went wrong")
                                try:
                                    myQ1 = nbId / sumTests
                                except Exception:
                                    myQ1 = np.nan
                                q1 = pd.concat([q1,pd.DataFrame({"Q1": myQ1},
                                    index=[0])], ignore_index=True)

                                ###
                                # Progres
                                print("Genetic pop. stats progress: " +
                                      str(round((my_step / nb_steps_hs) *
                                                100)) +
                                      "%   \r", end='', flush=True)
                                my_step += 1
    print("End of genetic stats computation at population level")

    popdata = hobs
    popdata["Hs"] = hs["Hs"]
    popdata["Fis"] = fis["Fis"]
    popdata["Q1"] = q1["Q1"]
    popdata["AllRich"] = allrich["AllRich"]
    popdata["Evenness"] = evenness["Evenness"]
    return popdata


def calcul_ht(data, nb_m_sel):
    """Computes Ht"""
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbMarks = int(infos[2])
    nbMkSel = int(infos[3])
    nbGens = infos[4]

    colGens = [i for i in data.columns if "Gen " in i]

    nbStepsHt = nbReps * nbMarks * nbGens

    sel_nsel = ["Sel" if x < nbMkSel else "NSel" for x in
                [i for i in range(nbMarks)]]
    ht = pd.DataFrame(columns=["Rep", "Mk", "SelSt", "Gen", "Ht"])
    my_step = 0
    for nrep in range(0, nbReps):
        for nmk in range(0, nbMarks):
            myMks = data[(data["Replicate"] == nrep) & (data["Marker"] == nmk)]
            heterozygotes = find_heterozygotes(myMks)
            homozygotes = find_homozygotes(myMks)
            mk_combs = myMks["Genotype"]
            list_alleles = [i for i in
                            np.unique("/".join(mk_combs).split("/"))]
            for ngen in range(0, nbGens):
                if ngen in LST_GENS:
                    myGen = myMks[["Population", "Marker", "Genotype",
                                   colGens[ngen]]]
                    total_inds = myGen[colGens[ngen]].sum()
                    if total_inds == 0:
                        ligne = pd.DataFrame({"Rep": nrep, "Mk": nmk,
                                 "SelSt": sel_nsel[nmk], "Gen": ngen,
                                 "Ht": np.nan}, index=[0])
                    else:
                        pi_pas_carres = []
                        for allele in list_alleles:
                            index_with_allele = [ind for ind, i in
                                                 enumerate(mk_combs) if allele
                                                 in i.split("/")]
                            somme_indiv = 0
                            for ind in index_with_allele:
                                if ind in homozygotes:
                                    # si ce sont les homozygotes
                                    somme_indiv += 2 *\
                                    myGen.iloc[ind][colGens[ngen]]
                                elif ind in heterozygotes:
                                    # si ce sont des heterozygotes
                                    somme_indiv +=\
                                        myGen.iloc[ind][colGens[ngen]]
                                else:
                                    # sinon...
                                    print("Something went wrong")
                            mypi = somme_indiv / (2 * total_inds)
                            pi_pas_carres.append(mypi)
                        pi_carres = [i * i for i in pi_pas_carres]
                        myht = 1 - np.nansum(pi_carres)
                        ligne = pd.DataFrame({"Rep": nrep, "Mk": nmk,
                                 "SelSt": sel_nsel[nmk], "Gen": ngen,
                                 "Ht": myht}, index=[0])
                        ht = pd.concat([ht, ligne], ignore_index=True)

                # ###
                # # Progres
                print("Genetic metapop. stats progress: " + str(round(
                    (my_step / nbStepsHt) * 100)) + "%   \r", end='',
                    flush=True)
                my_step += 1
    print("End of genetic stats computation at metapopopulation level")
    return ht


def calcul_Q2(data, nb_m_sel):
    """Computes Q2"""
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbMarks = int(infos[2])
    nbMkSel = int(infos[3])
    nbGens = infos[4]

    nbStepsQ2 = nbReps * nbMarks * nbPops * (nbPops - 1) * nbGens

    colGens = [i for i in data.columns if "Gen " in i]

    sel_nsel = ["Sel" if x < nbMkSel else "NSel" for x in [i for i in
                                                           range(nbMarks)]]
    q2 = pd.DataFrame(columns=["Rep", "Mk", "SelSt", "Pop1", "Pop2", "Gen",
                               "Q2"])
    my_step = 0
    for nrep in range(0, nbReps):
        for nmk in range(0, nbMarks):
            if nmk not in MKS_TO_GET:
                next
            for pop1 in range(0, nbPops):
                for pop2 in range(0, nbPops):
                    if pop1 != pop2 and (pop1 in POPS_TO_GET
                                         or pop2 in POPS_TO_GET):
                        myMks = data[(data["Replicate"] == nrep) &
                                     (data["Marker"] == nmk) &
                                     ((data["Population"] == pop1) |
                                      (data["Population"] == pop2))]
                        for ngen in range(0, nbGens):
                            if ngen in LST_GENS:
                                sumTests = 0
                                nbId = 0
                                genNb = colGens[ngen]
                                myGenBett = myMks[
                                    ["Population", "Genotype", genNb]]
                                mk_combs1 =\
                                    myGenBett[myGenBett["Population"] ==
                                              pop1]["Genotype"]
                                mk_combs2 =\
                                    myGenBett[myGenBett["Population"] ==
                                              pop2]["Genotype"]

                                genValues = pd.DataFrame(
                                    columns=["gt1", "gt2", "value"])
                                for gen1 in mk_combs1:
                                    for gen2 in mk_combs2:
                                        valID = int(gen1.split("/")[0] ==
                                                    gen2.split("/")[0]) +\
                                                int(gen1.split("/")[0] ==
                                                    gen2.split("/")[1]) +\
                                                int(gen1.split("/")[1] ==
                                                    gen2.split("/")[0]) +\
                                                int(gen1.split("/")[1] ==
                                                    gen2.split("/")[1])
                                        ligne = pd.DataFrame({"gt1": gen1,
                                                 "gt2": gen2,
                                                 "value": valID}, index=[0])
                                        genValues = pd.concat([genValues, ligne], ignore_index=True)

                                for gen1 in mk_combs1:
                                    for gen2 in mk_combs2:
                                        gval = int(genValues[
                                        (genValues["gt1"] == gen1) &
                                        (genValues["gt2"] == gen2)]["value"].iloc[0])
                                        eff = int(myGenBett[
                    (myGenBett["Population"] == pop1) &
                    (myGenBett["Genotype"] == gen1)][genNb].iloc[0]) *\
                    int(myGenBett[(myGenBett["Population"] == pop2) &
                                  (myGenBett["Genotype"] == gen2)][genNb].iloc[0]) * 2
                                        sumTests += eff * 4
                                        nbId += gval * eff
                                try:
                                    myQ2 = nbId / sumTests
                                except Exception:
                                    myQ2 = np.nan
                                ligne = pd.DataFrame({"Rep": nrep, "Pop1": pop1,
                                         "Pop2": pop2, "Mk": nmk,
                                         "SelSt": sel_nsel[nmk], "Gen": ngen,
                                         "Q2": myQ2}, index=[0])
                                q2 = pd.concat([q2, ligne], ignore_index=True)
                                # ###
                                # # Progres
                                print("Q2 stat progress:" + str(round(
                                    ((my_step / nbStepsQ2) * 100)
                                )) + "%   \r", end='', flush=True)
                                my_step += 1
    print("End of Q2 stat computation")
    return q2


def get_carrying_capacities(path):
    """Get carrying capacities of populations from the settings file"""
    with open(os.path.join(path, "setting.log"), encoding="UTF-8") as setFile:
        myCarrs = []
        while True:
            line = setFile.readline()
            if "Number of population:" in line:
                nbPops = int(line.strip().split(":")[1])
                line = setFile.readline()
                line = setFile.readline().strip()
                for _ in range(0, nbPops):
                    line = setFile.readline().strip()
                    lineSep = [i.strip() for i in line.split("|")]
                    myCarrs.append(int(lineSep[3]))
                return myCarrs
            if not line:
                break


def calcul_demo_metapop(data, nb_m_sel):
    """Computes the survival rate of the metapopulation"""
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbGens = infos[4]

    colGens = [i for i in data.columns if "Gen " in i]
    survRate = pd.DataFrame(columns=["Rep", "Gen", "SurvRate"])
    for nrep in range(0, nbReps):
        for ngen in range(0, nbGens):
            if ngen in LST_GENS:
                nbLivePops = 0
                for npop in range(0, nbPops):
                    mySample = data[(data["Replicate"] == nrep) &
                                    (data["Population"] == npop) &
                                    (data["Marker"] == 0)]
                    tot_inds = mySample[colGens[ngen]].sum()
                    if tot_inds > 0:
                        nbLivePops += 1
            ligne = pd.DataFrame({"Rep": nrep, "Gen": ngen, "SurvRate":
                nbLivePops / nbPops},index=[0])
            survRate = pd.concat([survRate, ligne], ignore_index=True)
    print("End of demographic stats computation at metapopulation level")
    return survRate


def calcul_demo_pop(data, nb_m_sel, path):
    """Computes the occupancy rate of the populations"""
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbGens = infos[4]

    myCarrs = get_carrying_capacities(path)

    colGens = [i for i in data.columns if "Gen " in i]
    occRate = pd.DataFrame(columns=["Rep", "Pop", "Gen", "OccRate"])

    for nrep in range(0, nbReps):
        for npop in range(0, nbPops):
            for ngen in range(0, nbGens):
                if ngen in LST_GENS:
                    mySample = data[(data["Replicate"] == nrep) &
                                    (data["Population"] == npop) &
                                    (data["Marker"] == 0)]
                    tot_inds = mySample[colGens[ngen]].sum()
                    if tot_inds == 0:
                        myocc = np.nan
                    else:
                        myocc = tot_inds / myCarrs[npop]
                    ligne = pd.DataFrame({"Rep": nrep, "Pop": npop, "Gen": ngen,
                             "OccRate": myocc}, index=[0])
                    occRate = pd.concat([occRate, ligne], ignore_index=True)
    print("End of demographic stats computation at population level")
    return occRate


def calcul_migr(data, nb_m_sel, path):
    """Computes migration informations (Event in/out and Number in/out)"""
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbGens = infos[4]

    my_step = 0

    myMigr = pd.read_csv(os.path.join(path, "numTransfertMigration.csv"))

    colGens = [i for i in myMigr.columns if "Gen " in i]

    migr = pd.DataFrame(columns=["Rep", "Pop", "Gen", "migrEventIn",
                                 "migrNbIn", "migrEventOut", "migrNbOut"])
    for nrep in range(0, nbReps):
        for npop in range(0, nbPops):
            for ngen in LST_GENS:
                mySampleIn = myMigr[(myMigr["Replicate"] == nrep) &
                                    (myMigr["Target"] == npop)]
                if len(mySampleIn) > 0:
                    nbIndIn = mySampleIn[colGens[ngen]].sum()
                    nbEventIn = sum(mySampleIn[colGens[ngen]] > 0)
                else:
                    nbIndIn = 0
                    nbEventIn = 0
                mySampleOut = myMigr[(myMigr["Replicate"] == nrep) &
                                     (myMigr["Source"] == npop)]
                if len(mySampleOut) > 0:
                    nbIndOut = mySampleOut[colGens[ngen]].sum()
                    nbEventOut = sum(mySampleOut[colGens[ngen]] > 0)
                else:
                    nbIndOut = 0
                    nbEventOut = 0
                ligne = pd.DataFrame({"Rep": nrep, "Pop": npop, "Gen": (ngen + 1),
                         "migrEventIn": nbEventIn, "migrNbIn": nbIndIn,
                         "migrEventOut": nbEventOut, "migrNbOut": nbIndOut},
                         index=[0])
                migr = pd.concat([migr, ligne], ignore_index=True)

                # ###
                # # Progres
                print("Migration stats progress: " + str(round(
                    (my_step / (nbReps * nbPops * nbGens)) * 100
                )) + "%   \r", end='', flush=True)
                my_step += 1
    print("End of migration stats computation")
    return migr


def calcul_colo(data, nb_m_sel, path):
    """Computes migration informations (Event in/out and Number in/out)"""
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbGens = infos[4]

    my_step = 0

    myColo = pd.read_csv(os.path.join(path, "numTransfertColonization.csv"))

    colGens = [i for i in myColo.columns if "Gen " in i]

    colo = pd.DataFrame(columns=["Rep", "Pop", "Gen", "coloEventIn",
                                 "coloNbIn", "coloEventOut", "coloNbOut"])
    for nrep in range(0, nbReps):
        for npop in range(0, nbPops):
            for ngen in LST_GENS:
                mySampleIn = myColo[(myColo["Replicate"] == nrep) &
                                    (myColo["Target"] == npop)]
                if len(mySampleIn) > 0:
                    nbIndIn = mySampleIn[colGens[ngen]].sum()
                    nbEventIn = sum(mySampleIn[colGens[ngen]] > 0)
                else:
                    nbIndIn = 0
                    nbEventIn = 0
                mySampleOut = myColo[(myColo["Replicate"] == nrep) &
                                     (myColo["Source"] == npop)]
                if len(mySampleOut) > 0:
                    nbIndOut = mySampleOut[colGens[ngen]].sum()
                    nbEventOut = sum(mySampleOut[colGens[ngen]] > 0)
                else:
                    nbIndOut = 0
                    nbEventOut = 0
                ligne = pd.DataFrame({"Rep": nrep, "Pop": npop, "Gen": (ngen + 1),
                         "coloEventIn": nbEventIn, "coloNbIn": nbIndIn,
                         "coloEventOut": nbEventOut, "coloNbOut": nbIndOut},
                         index=[0])
                colo = pd.concat([colo, ligne], ignore_index=True)

                # ###
                # # Progres
                print("Colonisation stats progress: " + str(
                    round((my_step / (nbReps * nbPops * nbGens)) * 100)) +
                    "% \r", end='', flush=True)
                my_step += 1
    print("End of colonisation stats computation")
    return colo


def write_gen_pop(myResHsHobs, my_path):
    """Writes results of genetic population stats"""
    myResHsHobs.to_csv(os.path.join(my_path, "resultatGenPop.csv"),
                       index=False)


def write_gen_metapop(myResHt, my_path):
    """Writes results of genetic metapopulation stats"""
    myResHt.to_csv(os.path.join(my_path, "resultatGenMetapop.csv"),
                   index=False)


def write_demo_pop(myResDemoPop, my_path):
    """Writes results of demographic population stats"""
    myResDemoPop.to_csv(os.path.join(my_path, "resultatDemoPop.csv"),
                        index=False)


def write_demo_metapop(myResDemoMetapop, my_path):
    """Writes results of demographic metapopulation stats"""
    myResDemoMetapop.to_csv(os.path.join(my_path, "resultatDemoMetapop.csv"),
                            index=False)


def write_migr(myResMigr, my_path):
    "Writes results of migration stats"
    myResMigr.to_csv(os.path.join(my_path, "resultatMigration.csv"),
                     index=False)


def write_colo(myResColo, my_path):
    """Writes results of colonisation stats"""
    myResColo.to_csv(os.path.join(my_path, "resultatColonisation.csv"),
                     index=False)


def write_q2(myResQ2, my_path):
    """Writes results of Q2"""
    myResQ2.to_csv(os.path.join(my_path, "resultatQ2.csv"), index=False)

# myreshshobs = calcul_hs_hobs(rawData, NB_MKSEL, NB_STEPS_HS)

try:
    statsCalcul = sys.argv[6].lower().split(",")

    if "hobs" in statsCalcul or "hs" in statsCalcul or "fis" in statsCalcul\
       or "q1" in statsCalcul or "allrich" in statsCalcul\
       or "evenness" in statsCalcul or "genpop" in statsCalcul\
       or "allgen" in statsCalcul or "all" in statsCalcul:
        myreshshobs = calcul_hs_hobs(rawData, NB_MKSEL, NB_STEPS_HS)
        write_gen_pop(myreshshobs, PATH)
        del myreshshobs

    if "ht" in statsCalcul or "genmetapop" in statsCalcul\
       or "allgen" in statsCalcul or "all" in statsCalcul:
        myresht = calcul_ht(rawData, NB_MKSEL)
        write_gen_metapop(myresht, PATH)
        del myresht

    if "q2" in statsCalcul or "genmetapop" in statsCalcul\
       or "allgen" in statsCalcul or "all" in statsCalcul:
        myresq2 = calcul_Q2(rawData, NB_MKSEL)
        write_q2(myresq2, PATH)
        del myresq2

    if "surv" in statsCalcul or "demometapop" in statsCalcul\
       or "alldemo" in statsCalcul or "all" in statsCalcul:
        myresdemometapop = calcul_demo_metapop(rawData, NB_MKSEL)
        write_demo_metapop(myresdemometapop, PATH)
        del myresdemometapop

    if "occ" in statsCalcul or "demopop" in statsCalcul\
       or "alldemo" in statsCalcul or "all" in statsCalcul:
        myresdemopop = calcul_demo_pop(rawData, NB_MKSEL, PATH)
        write_demo_pop(myresdemopop, PATH)
        del myresdemopop

    if "migr" in statsCalcul or "allTransfert" in statsCalcul\
       or "all" in statsCalcul:
        try:
            myresmigr = calcul_migr(rawData, NB_MKSEL, PATH)
            write_migr(myresmigr, PATH)
            del myresmigr
        except Exception:
            print("No migration data")

    if "colo" in statsCalcul or "alltransfert" in statsCalcul\
       or "all" in statsCalcul:
        try:
            myrescolo = calcul_colo(rawData, NB_MKSEL, PATH)
            write_colo(myrescolo, PATH)
            del myrescolo
        except Exception:
            print("No colonisation data")
except Exception:
    sys.exit("No stat was wigen to compute")

stop_time = time.time()
execLength = round(stop_time - start_time, 2)
print("{execLength} seconds")
