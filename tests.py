import os

import pytest

import pandas as pd
import numpy as np

import hashlib
def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

# Test Pop 0, Mk 0
print("hi")

def test_basique_pop():
    os.system("python3 ./CMPynalyser.py testsData 0 0 0 g genpop")
    dat = pd.read_csv("testsData/resultatGenPop.csv")

    assert [i for i in dat["Hobs"]] == [0.5, 0.25, 0.61, 0.4]
    assert [i for i in dat["Hs"]] == [0.5, 0.21875, 0.66665, 0.4837499999999999]
    assert [i for i in dat["Fis"]] == [0, -0.1428571428571428, 0.0849771244281106, 0.1731266149870799]
    assert [i for i in dat["SelSt"]] == ["NSel", "NSel", "NSel", "NSel"]

    del dat
    os.system("rm testsData/resultat*.csv")

def test_allelic_richness():
    os.system("python3 ./CMPynalyser.py testsData 0 p 0,2 g allrich")
    dat = pd.read_csv("testsData/resultatGenPop.csv")

    assert dat["AllRich"][9] == 3
    assert dat["AllRich"][11] == 1
    assert dat["AllRich"][12] == 2
    assert dat["AllRich"][13] == 0

    del dat
    os.system("rm testsData/resultat*.csv")

def test_evenness():
    os.system("python3 ./CMPynalyser.py testsData 0 p 0,2 g evenness")
    dat = pd.read_csv("testsData/resultatGenPop.csv")

    assert dat["Evenness"][0] == 1
    assert dat["Evenness"][1] == 0.5435644431995964
    assert dat["Evenness"][8] == 0.9999772058068171
    assert dat["Evenness"][11] == 0
    assert np.isnan(dat["Evenness"][13])

    del dat
    os.system("rm testsData/resultat*.csv")

def test_1sel_pop():
    os.system("python3 ./CMPynalyser.py testsData 1 0 0,1 g hs")
    dat = pd.read_csv("testsData/resultatGenPop.csv")

    assert [i for i in dat["SelSt"]] == ["Sel", "Sel", "NSel", "NSel", "Sel", "Sel", "NSel", "NSel"]

    del dat
    os.system("rm testsData/resultat*.csv")

def test_allele_fixe_pop():
    os.system("python3 ./CMPynalyser.py testsData 0 0 2 g genpop")
    dat = pd.read_csv("testsData/resultatGenPop.csv")

    assert dat["Hobs"][3] == 0
    assert dat["Hs"][3] == 0
    assert dat["Fis"][3] == 1

    del dat
    os.system("rm testsData/resultat*.csv")

def test_pop_eteinte_pop():
    os.system("python3 ./CMPynalyser.py testsData 0 1 m g genpop")
    dat = pd.read_csv("testsData/resultatGenPop.csv")

    assert np.isnan(dat["Hobs"].iloc[11])
    assert np.isnan(dat["Hs"].iloc[11])
    assert np.isnan(dat["Fis"].iloc[11])

    del dat
    os.system("rm testsData/resultat*.csv")

def test_basique_Mpop():
    os.system("python3 ./CMPynalyser.py testsData 0 0 0 g genMetapop")
    dat = pd.read_csv("testsData/resultatGenMetapop.csv")

    assert dat["Ht"].iloc[0] == 0.4108641975308642

    del dat
    os.system("rm testsData/resultat*.csv")

def test_survRate_basique():
    os.system("python3 ./CMPynalyser.py testsData 0 0 0 g surv")
    dat = pd.read_csv("testsData/resultatDemoMetapop.csv")

    assert dat["SurvRate"].iloc[0] == 1
    assert dat["SurvRate"].iloc[3] == 0.5

    del dat
    os.system("rm testsData/resultat*.csv")

def test_occRate_basique():
    os.system("python3 ./CMPynalyser.py testsData 0 0 0 g occ")
    dat = pd.read_csv("testsData/resultatDemoPop.csv")

    assert dat["OccRate"].iloc[0] == 0.1
    assert dat["OccRate"].iloc[2] == 2.4242424242424242
    assert np.isnan(dat["OccRate"].iloc[7])

    del dat
    os.system("rm testsData/resultat*.csv")

def test_migration():
    os.system("python3 ./CMPynalyser.py data 2 p m g migr")
    dat = pd.read_csv("data/resultatMigration.csv")

    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 0) & (dat["Gen"] == 11)]["migrNbIn"].iloc[0] == 0
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 0) & (dat["Gen"] == 12)]["migrNbIn"].iloc[0] == 1000
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 0) & (dat["Gen"] == 15)]["migrNbIn"].iloc[0] == 1100
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 0) & (dat["Gen"] == 11)]["migrEventIn"].iloc[0] == 0
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 0) & (dat["Gen"] == 12)]["migrEventIn"].iloc[0] == 1
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 0) & (dat["Gen"] == 15)]["migrEventIn"].iloc[0] == 2

    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 3) & (dat["Gen"] == 2)]["migrNbOut"].iloc[0] == 0
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 3) & (dat["Gen"] == 3)]["migrNbOut"].iloc[0] == 1000
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 3) & (dat["Gen"] == 7)]["migrNbOut"].iloc[0] == 1050
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 3) & (dat["Gen"] == 2)]["migrEventOut"].iloc[0] == 0
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 3) & (dat["Gen"] == 3)]["migrEventOut"].iloc[0] == 1
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 3) & (dat["Gen"] == 7)]["migrEventOut"].iloc[0] == 2

    del dat
    os.system("rm data/resultat*.csv")

def test_colonisation():
    os.system("python3 ./CMPynalyser.py data 2 p m g colo")
    dat = pd.read_csv("data/resultatColonisation.csv")

    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 2) & (dat["Gen"] == 11)]["coloNbIn"].iloc[0] == 0
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 2) & (dat["Gen"] == 12)]["coloNbIn"].iloc[0] == 1000
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 2) & (dat["Gen"] == 15)]["coloNbIn"].iloc[0] == 1100
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 2) & (dat["Gen"] == 11)]["coloEventIn"].iloc[0] == 0
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 2) & (dat["Gen"] == 12)]["coloEventIn"].iloc[0] == 1
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 2) & (dat["Gen"] == 15)]["coloEventIn"].iloc[0] == 2

    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 5) & (dat["Gen"] == 2)]["coloNbOut"].iloc[0] == 0
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 5) & (dat["Gen"] == 3)]["coloNbOut"].iloc[0] == 1000
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 5) & (dat["Gen"] == 7)]["coloNbOut"].iloc[0] == 1050
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 5) & (dat["Gen"] == 2)]["coloEventOut"].iloc[0] == 0
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 5) & (dat["Gen"] == 3)]["coloEventOut"].iloc[0] == 1
    assert dat[(dat["Rep"] == 0) & (dat["Pop"] == 5) & (dat["Gen"] == 7)]["coloEventOut"].iloc[0] == 2

    del dat
    os.system("rm data/resultat*.csv")

def test_probIDQ1():
    os.system("python3 ./CMPynalyser.py testsProbID 1 p m g Q1") #
    dat = pd.read_csv("testsProbID/resultatGenPop.csv")

    assert float(dat[(dat["Rep"] == 0) & (dat["Pop"] == 0) & (dat["Mk"] == 0) &
        (dat["Gen"] == 0)]["Q1"].iloc[0]) == 0.4777777777777778
    assert float(dat[(dat["Rep"] == 0) & (dat["Pop"] == 0) & (dat["Mk"] == 0) & (dat["Gen"] == 1)]["Q1"].iloc[0]) == 0.4888888888888889
    assert float(dat[(dat["Rep"] == 0) & (dat["Pop"] == 0) & (dat["Mk"] == 1) & (dat["Gen"] == 0)]["Q1"].iloc[0]) == 0.3


    del dat
    os.system("rm testsProbID/resultat*.csv")

def test_probIDQ2():
    os.system("python3 ./CMPynalyser.py testsProbID 1 p m g Q2")
    dat = pd.read_csv("testsProbID/resultatQ2.csv")

    assert float(dat[(dat["Rep"] == 0) & (dat["Pop1"] == 0) & (dat["Pop2"] == 1) & (dat["Mk"] == 0) & (dat["Gen"] == 0)]["Q2"].iloc[0]) == 0.5
    assert float(dat[(dat["Rep"] == 0) & (dat["Pop1"] == 0) & (dat["Pop2"] == 1) & (dat["Mk"] == 1) & (dat["Gen"] == 0)]["Q2"].iloc[0]) == 0.325

    del dat
    os.system("rm testsProbID/resultat*.csv")
